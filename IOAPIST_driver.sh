#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### A top-level driver for checking spatiotemporality (ST) of IOAPI files.
### See https://bitbucket.org/epa_n2o_project_team/ioapi_spatiotemporality_checker

### Expects to run on linux. Dependencies include:
### * GNU utilities: `basename`, `dirname`, `readlink`, (for testing) `mktemp`
### * `true` and `false` for "real bash booleans"
### * NCL. Not a dependency of this script per se, but it expects to call NCL scripts.

### Note: if ${THIS} not run from uber_driver.sh, you may need to set ${GIT_CLONE_PREFIX} on some platforms (e.g., EMVL/terrae). 
### See uber_driver.sh for details.

### Configure as needed for your platform.

# ----------------------------------------------------------------------
# constants part 1: simple manipulations
# ----------------------------------------------------------------------

### logging

# TODO: take switches for help, debugging, no/eval, target drive
THIS="$0"
THIS_FN="$(basename ${THIS})"
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

# note: following will fail if `source`ing!
source "${THIS_DIR}/resource_strings.sh" # allow reuse in uber_driver.sh, etc

### helpers

## helpers in this repo. TODO: move generic code to regrid_utils.

IOAPIST_HELPERS_FN='IOAPIST_helpers.ncl'
export IOAPIST_HELPERS_FP="${WORK_DIR}/${IOAPIST_HELPERS_FN}"

IOAPIST_SINGLEFILE_TESTER_FN='IOAPIST_ref_vs_test.ncl'
export IOAPIST_SINGLEFILE_TESTER_FP="${WORK_DIR}/${IOAPIST_SINGLEFILE_TESTER_FN}"

IOAPIST_LISTFILE_TESTER_FN='IOAPIST_reflist_vs_testlist.ncl'
export IOAPIST_LISTFILE_TESTER_FP="${WORK_DIR}/${IOAPIST_LISTFILE_TESTER_FN}"

# ## helpers retrieved from elsewhere. TODO: create NCL and R packages

# # path to a folder, not a file: needed by NCL to get to initial helpers
# export REGRID_UTILS_DIR="${WORK_DIR}/${REGRID_UTILS_PN}" # folder, not file

# ## TODO: move these back to regrid_utils
# ## But allow user to override repo code: see (e.g.) `get_bash_utils`

# export ATTR_FUNCS_FN='attributes.ncl'
# export ATTR_FUNCS_FP="${WORK_DIR}/${ATTR_FUNCS_FN}"

# export BASH_UTILS_FN='bash_utilities.sh'
# export BASH_UTILS_FP="${WORK_DIR}/${BASH_UTILS_FN}"

# export COMPARE_FUNCS_FN='compare.ncl'
# export COMPARE_FUNCS_FP="${WORK_DIR}/${COMPARE_FUNCS_FN}"

# export DIM_FUNCS_FN='dimensions.ncl'
# export DIM_FUNCS_FP="${WORK_DIR}/${DIM_FUNCS_FN}"

# export FILEPATH_FUNCS_FN='get_filepath_from_template.ncl'
# export FILEPATH_FUNCS_FP="${WORK_DIR}/${FILEPATH_FUNCS_FN}"

# export IOAPI_FUNCS_FN='IOAPI.ncl'
# export IOAPI_FUNCS_FP="${WORK_DIR}/${IOAPI_FUNCS_FN}"

### commandline options

## Use "real bash booleans" for which alternate values are (executables, not strings, so not quoted) true , false
NO_HORIZONTAL=false
NO_VERTICAL=false
NO_SPATIAL=false
NO_TEMPORAL=false

PROCESS_FILES=false
PROCESS_LISTFILES=false

## file paths ('fp'), which can point to files which are themselves lists of filepaths (listfiles) or just simple IOAPI/netCDF files

REFERENCE_FP=''
LIST_OF_REFR_FILES_FP=''
TEST_FP=''
LIST_OF_TEST_FILES_FP=''

FOUND_VALID_ARG=false # none so far!

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# setup functions
# ----------------------------------------------------------------------

### Assist user flailing at commandline option setting.
function print_usage {
  # USAGE not local--should be?
  local THIS_FN_SPACE="$(echo -e "${THIS_FN}" | tr 'a-zA-Z0-9./_-~' ' ')" # same length as THIS_FN, but all-space
  USAGE="
${THIS_FN} compares a test file (or each member of a list) to a reference file (or each member of a list).
One (but not both) of the pairs (reference file, test file), (reference-files listfile, test-files listfile) is required.

Usage: ${THIS_FN} [-h|--help] [-H|--no-horizontal] [-V|--no-vertical] [-S|--no-spatial] [-T|--no-temporal]
       ${THIS_FN_SPACE} [-r|--rf|--reference-file] [path to ref file] [-t|--tf|--test-file] [path to test file] 
  -h ,  --help                   : print this information
  -r ,  --reference-file , --rf  : FQ path to reference or 'golden' file to which to compare
        --reference-list , --rl  : FQ path to list of reference files, one per line (no short option)
  -t ,  --test-file , --tf       : FQ path to file to test against reference
        --test-list , --tl       : FQ path to list of files to test, one per line (no short option)
  -H ,  --no-horizontal          : do not test horizontal attributes
  -V ,  --no-vertical            : do not test vertical attributes
  -S ,  --no-spatial             : do not test spatial attributes (implies --no-horizontal && --no-vertical)
  -T ,  --no-temporal            : do not test temporal attributes
"
  echo -e "${USAGE}" >&2
} # end function print_usage

### Assist ourselves in debugging commandline option setting.
function print_options {
  echo -e "${MESSAGE_PREFIX} NO_HORIZONTAL          ='${NO_HORIZONTAL}'"
  echo -e "${MESSAGE_PREFIX} NO_VERTICAL            ='${NO_VERTICAL}'"
  echo -e "${MESSAGE_PREFIX} NO_SPATIAL             ='${NO_SPATIAL}'"
  echo -e "${MESSAGE_PREFIX} NO_TEMPORAL            ='${NO_TEMPORAL}'"
  echo -e "${MESSAGE_PREFIX} PROCESS_FILES          ='${PROCESS_FILES}'"
  echo -e "${MESSAGE_PREFIX} PROCESS_LISTFILES      ='${PROCESS_LISTFILES}'"
  echo -e "${MESSAGE_PREFIX} REFERENCE_FP           ='${REFERENCE_FP}'"
  echo -e "${MESSAGE_PREFIX} TEST_FP                ='${TEST_FP}'"
  echo -e "${MESSAGE_PREFIX} LIST_OF_REFR_FILES_FP  ='${LIST_OF_REFR_FILES_FP}'"
  echo -e "${MESSAGE_PREFIX} LIST_OF_TEST_FILES_FP  ='${LIST_OF_TEST_FILES_FP}'"
} # end function print_options

### Do whatever is necessary at the start of a run.
function setup {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
  local LOOP_MESSAGE_PREFIX
  local LOOP_ERROR_PREFIX

  # note `setup_resources` is in resource_strings for sharing with uber_driver
  for CMD in \
    'evaluate_options' \
    'setup_resources' \
    'get_helpers' \
  ; do
    LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
    LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
    echo -e "\n${LOOP_MESSAGE_PREFIX}\n"
    eval "${CMD}"
    if [[ $? -ne 0 ]] ; then
      echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
      exit 2
    fi
  done

} # end function setup

### What has the user done to our CLI?
function evaluate_options {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if   ! ${FOUND_VALID_ARG} ; then # note "real bash booleans" have "naked tests"
    echo -e "${ERROR_PREFIX} found no valid arguments, exiting ..."
    print_usage
    exit 3
  elif ! ${PROCESS_FILES} && ! ${PROCESS_LISTFILES} ; then
    echo -e "${ERROR_PREFIX} will not process either simple files or listfiles, so nothing to do! exiting ..."
    print_usage
    exit 4
  elif [[ -n "${REFERENCE_FP}" && -z "${TEST_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} specified path to reference file without path to test file, exiting ..."
    print_usage
    exit 5
  elif [[ -z "${REFERENCE_FP}" && -n "${TEST_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} specified path to test file without path to reference file, exiting ..."
    print_usage
    exit 6
  elif [[ -n "${LIST_OF_REFR_FILES_FP}" && -z "${LIST_OF_TEST_FILES_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} specified path to list of reference files without path to list of test files, exiting ..."
    print_usage
    exit 7
  elif [[ -z "${LIST_OF_REFR_FILES_FP}" && -n "${LIST_OF_TEST_FILES_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} specified path to list of test files without path to list of reference files, exiting ..."
    print_usage
    exit 8
  elif ( ${NO_SPATIAL} && ! ${NO_HORIZONTAL} && ! ${NO_VERTICAL} ) ||
       ( ${NO_SPATIAL} &&   ${NO_HORIZONTAL} && ! ${NO_VERTICAL} ) ||
       ( ${NO_SPATIAL} && ! ${NO_HORIZONTAL} &&   ${NO_VERTICAL} )    ; then
    echo -e "${ERROR_PREFIX} no spatial processing implies NEITHER horizontal nor vertical processing, exiting ..."
    exit 9
  elif ${NO_SPATIAL} && ${NO_TEMPORAL} ; then
    echo -e "${ERROR_PREFIX} will not process spatially or temporally, so nothing to do! exiting ..."
    print_usage
    exit 10
  fi
  print_options # debugging: see what we've set

} # end function evaluate_options

### Get needed helper modules.
### Note: if used, 'get_regrid_utils' must go first, since latter rely on it.
function get_helpers {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
  local LOOP_MESSAGE_PREFIX
  local LOOP_ERROR_PREFIX

#     'get_regrid_utils' \
#     'get_IOAPIST_helpers' \
#     'get_singlefile_driver' \
#     'get_listfile_driver' \
  for CMD in \
    'get_IOAPIST_helpers' \
    'get_singlefile_driver' \
    'get_listfile_driver' \
  ; do
    LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
    LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
    echo -e "\n${LOOP_MESSAGE_PREFIX}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
      exit 11
    fi
  done
} # end function get_helpers

### !isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
### To override, copy/mod to ${IOAPIST_HELPERS_FP} before running this script.
function get_IOAPIST_helpers {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${IOAPIST_HELPERS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} IOAPIST_HELPERS_FP not defined"
    exit 12
  fi
  # is in this repo, not (yet?) a regrid_utils
#   if [[ ! -r "${IOAPIST_HELPERS_FP}" ]] ; then
#     # copy from downloaded regrid_utils
#     for CMD in \
#       "cp ${REGRID_UTILS_DIR}/${BC_IC_FUNCS_FN} ${IOAPIST_HELPERS_FP}" \
#     ; do
#       echo -e "$ ${CMD}"
#       eval "${CMD}"
#     done
#   fi
  if [[ ! -r "${IOAPIST_HELPERS_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} path to IOAPIST helpers=='${IOAPIST_HELPERS_FP}' not readable"
    exit 13
  fi
} # end function get_IOAPIST_helpers

### !isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
### To override, copy/mod to ${IOAPIST_SINGLEFILE_TESTER_FP} before running this script.
function get_singlefile_driver {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${IOAPIST_SINGLEFILE_TESTER_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} IOAPIST_SINGLEFILE_TESTER_FP not defined"
    exit 14
  fi
  # is in this repo, not (yet?) a regrid_utils
#   if [[ ! -r "${IOAPIST_SINGLEFILE_TESTER_FP}" ]] ; then
#     # copy from downloaded regrid_utils
#     for CMD in \
#       "cp ${REGRID_UTILS_DIR}/${BC_IC_FUNCS_FN} ${IOAPIST_SINGLEFILE_TESTER_FP}" \
#     ; do
#       echo -e "$ ${CMD}"
#       eval "${CMD}"
#     done
#   fi
  if [[ ! -r "${IOAPIST_SINGLEFILE_TESTER_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} path to IOAPIST single-file processor=='${IOAPIST_SINGLEFILE_TESTER_FP}' not readable"
    exit 15
  fi
} # end function get_singlefile_driver

### !isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
### To override, copy/mod to ${IOAPIST_LISTFILE_TESTER_FP} before running this script.
function get_listfile_driver {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  if [[ -z "${IOAPIST_LISTFILE_TESTER_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} IOAPIST_LISTFILE_TESTER_FP not defined"
    exit 16
  fi
  # is in this repo, not (yet?) a regrid_utils
#   if [[ ! -r "${IOAPIST_LISTFILE_TESTER_FP}" ]] ; then
#     # copy from downloaded regrid_utils
#     for CMD in \
#       "cp ${REGRID_UTILS_DIR}/${BC_IC_FUNCS_FN} ${IOAPIST_LISTFILE_TESTER_FP}" \
#     ; do
#       echo -e "$ ${CMD}"
#       eval "${CMD}"
#     done
#   fi
  if [[ ! -r "${IOAPIST_LISTFILE_TESTER_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} path to IOAPIST listfile processor=='${IOAPIST_LISTFILE_TESTER_FP}' not readable"
    exit 17
  fi
} # end function get_listfile_driver

# ----------------------------------------------------------------------
# action functions
# ----------------------------------------------------------------------

### Evaluate parsed commandline, sets global envvar=NCL_RUNNER accordingly for payload
function parse_inputs {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
#  local CMD=''

  if   [[ -z "${LIST_OF_REFR_FILES_FP}" && -n "${LIST_OF_TEST_FILES_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} LIST_OF_REFR_FILES_FP not defined, exiting"
    exit 18
  elif [[ -n "${LIST_OF_REFR_FILES_FP}" && -z "${LIST_OF_TEST_FILES_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} LIST_OF_TEST_FILES_FP not defined, exiting"
    exit 19
  elif [[ -n "${LIST_OF_REFR_FILES_FP}" && -n "${LIST_OF_TEST_FILES_FP}" ]] ; then # process the files!
#    export NCL_RUNNER='check_refr_vs_test_listfiles' # fails--we're in subshell
    echo -e 'check_refr_vs_test_listfiles' # instead, return bash-style
  # Following clauses assume [[ -z "${LIST_OF_REFR_FILES_FP}" && -z "${LIST_OF_TEST_FILES_FP}" ]]
  elif [[ -z "${REFERENCE_FP}" && -n "${TEST_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} REFERENCE_FP not defined, exiting"
    exit 20
  elif [[ -n "${REFERENCE_FP}" && -z "${TEST_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} TEST_FP not defined, exiting"
    exit 21
  elif [[ -n "${REFERENCE_FP}" && -n "${TEST_FP}" ]] ; then
#    export NCL_RUNNER='check_refr_vs_test_filepaths' # fails--we're in subshell
    echo -e 'check_refr_vs_test_filepaths' # instead, return bash-style
  else # none defined
    echo -e "${ERROR_PREFIX} must define one pair of either {REFERENCE_FP, TEST_FP} or {LIST_OF_REFR_FILES_FP, LIST_OF_TEST_FILES_FP}, exiting"
    exit 22
  fi
} # end function parse_inputs

### Compose an NCL commandline from a kernel adding our (global) test gates (if set/true)
#(command_composer "REFERENCE_FP='${REFERENCE_FP}' TEST_FP='${TEST_FP}' ncl -n ${IOAPIST_SINGLEFILE_TESTER_FP}")"
function command_composer {
  local CMD="${1}"

  ## prevent duplication
  if ${NO_SPATIAL} ; then
    CMD="NO_HORIZONTAL='True' ${CMD}"
    CMD="NO_VERTICAL='True' ${CMD}"
    # prevent reprocessing
    NO_HORIZONTAL=false
    NO_VERTICAL=false
  fi

  if ${NO_HORIZONTAL} ; then
    CMD="NO_HORIZONTAL='True' ${CMD}"
  fi

  if ${NO_VERTICAL} ; then
#    CMD="NO_VERTICAL='True' ${CMD}"
# want type=logical
    CMD="NO_VERTICAL=True ${CMD}"
  fi

  if ${NO_TEMPORAL} ; then
    CMD="NO_TEMPORAL='True' ${CMD}"
  fi

  echo -e "${CMD}" # bash-style return :-(
} # end function command_composer

### Call NCL to compare each paths in 2 lists of filepaths.
### Assumes `ncl` is in PATH. TODO: use regrid_utils/bash_utilities.sh
function check_refr_vs_test_listfiles {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
  local NCL_CMD='' # to become the NCL commandline

  if   [[ -z "${LIST_OF_REFR_FILES_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} LIST_OF_REFR_FILES_FP not defined, exiting"
    exit 24
  elif [[ ! -r "${LIST_OF_REFR_FILES_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read list of reference files='${LIST_OF_REFR_FILES_FP}', exiting"
    exit 25
  elif [[ -z "${LIST_OF_TEST_FILES_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} LIST_OF_TEST_FILES_FP not defined, exiting"
    exit 26
  elif [[ ! -r "${LIST_OF_TEST_FILES_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read list of test files='${LIST_OF_TEST_FILES_FP}', exiting"
    exit 27
  else # process the files!
#    ncl # bail to NCL and copy script lines, or ...
    # Compose an NCL commandline.
    NCL_CMD="$(command_composer "LIST_OF_REFR_FILES_FP='${LIST_OF_REFR_FILES_FP}' LIST_OF_TEST_FILES_FP='${LIST_OF_TEST_FILES_FP}' ncl -n ${IOAPIST_LISTFILE_TESTER_FP}")"
    # Run the NCL commandline.
    for CMD in "${NCL_CMD}" ; do
      LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
      LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
      cat <<EOM

About to run command="${CMD}"

EOM
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
        exit 28
      fi
    done
  fi # [[ -z "${LIST_OF_REFR_FILES_FP}" ]]

} # end function check_refr_vs_test_listfiles

### Call NCL to compare paths to 2 files.
### Assumes `ncl` is in PATH. TODO: use regrid_utils/bash_utilities.sh
function check_refr_vs_test_filepaths {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
  local NCL_CMD='' # to become the NCL commandline

  if   [[ -z "${REFERENCE_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} REFERENCE_FP not defined, exiting"
    exit 29
  elif [[ ! -r "${REFERENCE_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read reference file='${REFERENCE_FP}', exiting"
    exit 30
  elif [[ -z "${TEST_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} TEST_FP not defined, exiting"
    exit 31
  elif [[ ! -r "${TEST_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot read file under test='${TEST_FP}', exiting"
    exit 32
  else # process the files!
#    ncl # bail to NCL and copy script lines, or ...
    # Compose an NCL commandline.
    NCL_CMD="$(command_composer "REFERENCE_FP='${REFERENCE_FP}' TEST_FP='${TEST_FP}' ncl -n ${IOAPIST_SINGLEFILE_TESTER_FP}")"
    for CMD in "${NCL_CMD}"; do
      LOOP_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${CMD}:"
      LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
      cat <<EOM

About to run command="${CMD}"

EOM
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "${LOOP_ERROR_PREFIX} failed or not found\n"
        exit 33
      fi
    done
  fi # [[ -z "${REFERENCE_FP}" ]]

} # end function check_refr_vs_test_filepaths

### Do whatever is necessary at the end of a run.
function teardown {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

#  echo -e "${MESSAGE_PREFIX} nothing to do"
} # end function teardown

# ----------------------------------------------------------------------
# constants part 2: parse commandline
# ----------------------------------------------------------------------

### TODO: should be part of function=evaluate_options? dunno how `getopts` would handle that

OPTSPEC=":hHrStTV-:"
while getopts "$OPTSPEC" OPTCHAR; do
  case "${OPTCHAR}" in
    -)
      case "${OPTARG}" in
        help)
          FOUND_VALID_ARG=true
          print_usage
          exit 0 # not an error, but we don't wanna print_options either
          ;;
# works!
#         ll|loglevel)
#           val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
#           echo "Parsing option: '--${OPTARG}', value: '${val}'"
#           ;;
        rf|reference-file)
          FOUND_VALID_ARG=true
          REFERENCE_FP="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
#          echo "Parsing option: '--${OPTARG}', value='${REFERENCE_FP}'" # debugging
          if [[ -z "${REFERENCE_FP}" ]] ; then
            echo -e "${ERROR_PREFIX} set option='reference-file' without giving path to reference file, exiting ..."
            exit 34
          else
            # defer checking path til later
            PROCESS_FILES=true
            PROCESS_LISTFILES=false
          fi
          ;;
        tf|test-file)
          FOUND_VALID_ARG=true
          TEST_FP="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
#          echo "Parsing option: '--${OPTARG}', value='${TEST_FP}'" # debugging
          if [[ -z "${TEST_FP}" ]] ; then
            echo -e "${ERROR_PREFIX} set option='test-file' without giving path to test file, exiting ..."
            exit 35
          else
            # defer checking path til later
            PROCESS_FILES=true
            PROCESS_LISTFILES=false
          fi
          ;;
        rl|reference-list)
          FOUND_VALID_ARG=true
          LIST_OF_REFR_FILES_FP="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
#          echo "Parsing option: '--${OPTARG}', value='${LIST_OF_REFR_FILES_FP}'" # debugging
          if [[ -z "${LIST_OF_REFR_FILES_FP}" ]] ; then
            echo -e "${ERROR_PREFIX} set option='reference-list' without giving path to list of reference files, exiting ..."
            exit 36
          else
            # defer checking path til later
            PROCESS_FILES=false
            PROCESS_LISTFILES=true
          fi
          ;;
        tl|test-list)
          FOUND_VALID_ARG=true
          LIST_OF_TEST_FILES_FP="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
#          echo "Parsing option: '--${OPTARG}', value='${LIST_OF_TEST_FILES_FP}'" # debugging
          if [[ -z "${LIST_OF_TEST_FILES_FP}" ]] ; then
            echo -e "${ERROR_PREFIX} set option='test-list' without giving path to list of test files, exiting ..."
            exit 37
          else
            # defer checking path til later
            PROCESS_FILES=false
            PROCESS_LISTFILES=true
          fi
          ;;
        no-horizontal)
          FOUND_VALID_ARG=true
#           val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
#           echo "Parsing option: '--${OPTARG}', value: '${val}'"
          NO_HORIZONTAL=true
          ;;
        no-vertical)
          FOUND_VALID_ARG=true
#           val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
#           echo "Parsing option: '--${OPTARG}', value: '${val}'"
          NO_VERTICAL=true
          ;;
        no-spatial)
          FOUND_VALID_ARG=true
#           val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
#           echo "Parsing option: '--${OPTARG}', value: '${val}'"
          NO_SPATIAL=true
          # which implies
          NO_HORIZONTAL=true
          NO_VERTICAL=true
          ;;
        no-temporal)
          FOUND_VALID_ARG=true
#           val="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
#           echo "Parsing option: '--${OPTARG}', value: '${val}'"
          NO_TEMPORAL=true
          ;;
## From original, shows how to parse longopt=value
#         loglevel=*)
#           val=${OPTARG#*=}
#           opt=${OPTARG%=$val}
#           echo "Parsing option: '--${opt}', value: '${val}'"
#           ;;
        *)
          if [ "$OPTERR" = 1 ] && [ "${OPTSPEC:0:1}" != ":" ]; then
            echo "${ERROR_PREFIX} unknown option --${OPTARG}, exiting ..." >&2
            print_usage
            exit 38
          fi
          ;;
      esac;;
    h)
      FOUND_VALID_ARG=true
      print_usage
      exit 0 # not an error, but we don't wanna print_options either
      ;;
    H)
      FOUND_VALID_ARG=true
#      echo "Parsing option: '-${OPTCHAR}'" # debugging
      NO_HORIZONTAL=true
      ;;
    r)
      FOUND_VALID_ARG=true
#      echo "Parsing option: '-${OPTCHAR}'" # debugging
      REFERENCE_FP="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
      if [[ -z "${REFERENCE_FP}" ]] ; then
        echo -e "${ERROR_PREFIX} set option='reference-file' without giving path to reference file, exiting ..."
        exit 39
      else
        # defer checking path til later
        PROCESS_FILES=true
        PROCESS_LISTFILES=false
      fi
      ;;
    S)
      FOUND_VALID_ARG=true
#      echo "Parsing option: '-${OPTCHAR}'" # debugging
      NO_SPATIAL=true
      # which implies
      NO_HORIZONTAL=true
      NO_VERTICAL=true
      ;;
    t)
      FOUND_VALID_ARG=true
#      echo "Parsing option: '-${OPTCHAR}'" # debugging
      TEST_FP="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
      if [[ -z "${TEST_FP}" ]] ; then
        echo -e "${ERROR_PREFIX} set option='test-file' without giving path to test file, exiting ..."
        exit 40
      else
        # defer checking path til later
        PROCESS_FILES=true
        PROCESS_LISTFILES=false
      fi
      ;;
    T)
      FOUND_VALID_ARG=true
#      echo "Parsing option: '-${OPTCHAR}'" # debugging
      NO_TEMPORAL=true
      ;;
    V)
      FOUND_VALID_ARG=true
#      echo "Parsing option: '-${OPTCHAR}'" # debugging
      NO_VERTICAL=true
      ;;
    *)
      if [ "$OPTERR" != 1 ] || [ "${OPTSPEC:0:1}" = ":" ]; then
        echo "${ERROR_PREFIX} non-option argument: '-${OPTARG}', exiting ..." >&2
        print_usage
        exit 41
      fi
      ;;
  esac
done

# then a bit of cleanup, to avoid latenight logic problems :-)

if   ${NO_HORIZONTAL} && ${NO_VERTICAL} ; then
  NO_SPATIAL=true
elif ${NO_SPATIAL} ; then
  NO_HORIZONTAL=true
  NO_VERTICAL=true
fi

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

### TODO: make this run in one loop!

#   'setup' \             # paths, helpers, etc
#   'parse_inputs' \      # parses commandline, checks some args, sets NCL_RUNNER
#   ${NCL_RUNNER} \       # call NCL for heavy lifting     
#   'teardown' \          # cleanup
# should always
# * begin with `setup` to do `module add`, etc
# * end with `teardown` for tidy and testing (e.g., plot display)

for CMD in \
  'setup' \
; do
  LOOP_MESSAGE_PREFIX="${THIS_FN}::main loop::${CMD}:"
  LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
  echo -e "\n${LOOP_MESSAGE_PREFIX}\n"
  eval "${CMD}" # comment this out for NOPing, e.g., to `source`
  if [[ $? -ne 0 ]] ; then
    echo -e "\n${LOOP_ERROR_PREFIX} failed or not found\n"
    exit 42
  fi
done

NCL_RUNNER="$(parse_inputs)"

for CMD in \
  "${NCL_RUNNER}" \
  'teardown' \
; do
  LOOP_MESSAGE_PREFIX="${THIS_FN}::main loop::${CMD}:"
  LOOP_ERROR_PREFIX="${LOOP_MESSAGE_PREFIX} ERROR:"
  echo -e "\n${LOOP_MESSAGE_PREFIX}\n"
  eval "${CMD}" # comment this out for NOPing, e.g., to `source`
  if [[ $? -ne 0 ]] ; then
    echo -e "\n${LOOP_ERROR_PREFIX} failed or not found\n"
    exit 42
  fi
done

exit 0 # success! and prevents control from flowing past this point

# ----------------------------------------------------------------------
# testing
# ----------------------------------------------------------------------

### Currently to be run "by hand" from console, since script will `exit` before it gets here.
### TODO: an oracle to automate these tests!
### TODO: automock data (e.g., to test/ensure particular failure modes)
### TODO: (of course) add any tests you fail in future as test cases.

# ----------------------------------------------------------------------
# test fixture
# ----------------------------------------------------------------------

### paths, bash on infinity
THIS_FP='./IOAPIST_driver.sh' # set BY HAND to ensure it matches where you are!
RUN_DIR="${HOME}/code/regridding/IOAPI_spatiotemporality_checker" # FQ path to where you test
pushd ${RUN_DIR}
if [[ ! -x "${THIS_FP}" ]] ; then
  echo -e "ERROR: cannot run driver='${THIS_FP}', exiting ..."
  exit 1
fi

## Ensure "real" IOAPI/netCDF files do not exist at these paths
FAKE_FP_1="$(mktemp)"
rm ${FAKE_FP_1} # overkill?
FAKE_FP_2="$(mktemp)"
rm ${FAKE_FP_2}

## These paths to simple files must be good, but the two files themselves should be mutually incompatible.
## But a file will always be compatible with itself, so we can also test compatibility.
## Note these particular files are only incompatible vertically, so temporal and horizontal tests should pass.
INCOMP_FILE_FP_1='/project/inf35w/roche/met/24-layer/WRF_2008_24aL/12US1/mcip_out/METBDY3D_071222'
INCOMP_FILE_FP_2='/project/inf35w/roche/BC/2008cdc/AQ_raw/BCON_CB05AE5_US12_2007356'

if [[ -r "${INCOMP_FILE_FP_1}" && -r "${INCOMP_FILE_FP_2}" ]] ; then
  # Mock short listfiles for testing
  INCOMP_LISTFILE_FP_1="${RUN_DIR}/refr_files.txt"
  INCOMP_LISTFILE_FP_2="${RUN_DIR}/test_files.txt"
  echo -e "${INCOMP_FILE_FP_1}" > ${INCOMP_LISTFILE_FP_1}
  echo -e "${INCOMP_FILE_FP_2}" > ${INCOMP_LISTFILE_FP_2}
  if [[ ! -r "${INCOMP_LISTFILE_FP_1}" || ! -r "${INCOMP_LISTFILE_FP_2}" ]] ; then
    echo -e "ERROR: cannot read both list files:"
    ls -al ${INCOMP_LISTFILE_FP_1}
    ls -al ${INCOMP_LISTFILE_FP_2}
    echo -e "exiting ...\n"
    exit 2
  fi
else
  echo -e "ERROR: cannot read both simple files:"
  ls -al ${INCOMP_FILE_FP_1}
  ls -al ${INCOMP_FILE_FP_2}
  echo -e "exiting ...\n"
  exit 3
fi

# ----------------------------------------------------------------------
# test cases
# ----------------------------------------------------------------------

bash --version | head -n 1

${THIS_FP}

${THIS_FP} -f

${THIS_FP} -f foo

${THIS_FP} foo

${THIS_FP} foo bar

${THIS_FP} --very-bad

${THIS_FP} --Very-bad

${THIS_FP} -h

${THIS_FP} --help

${THIS_FP} --rf ${FAKE_FP_1}

${THIS_FP} -rf ${FAKE_FP_1}

${THIS_FP} -r ${FAKE_FP_1}

${THIS_FP} --tf ${FAKE_FP_2}

${THIS_FP} -tf ${FAKE_FP_2}

${THIS_FP} -t ${FAKE_FP_2}

${THIS_FP} --rf ${FAKE_FP_1} --tf ${FAKE_FP_2}

${THIS_FP} --rf ${INCOMP_FILE_FP_1} --tf ${FAKE_FP_2}

${THIS_FP} --rf ${FAKE_FP_1} -t ${INCOMP_FILE_FP_2}

${THIS_FP} -r ${INCOMP_FILE_FP_1} --tf ${INCOMP_FILE_FP_1}

${THIS_FP} -r ${INCOMP_FILE_FP_1} -t ${INCOMP_FILE_FP_2}

${THIS_FP} -tl ${FAKE_FP_1}

${THIS_FP} --rl ${FAKE_FP_1}

${THIS_FP} --tl ${FAKE_FP_2}

${THIS_FP} --rl ${FAKE_FP_1} --tl ${FAKE_FP_2}

${THIS_FP} --rf ${FAKE_FP_1} --tl ${FAKE_FP_2}

${THIS_FP} --rl ${FAKE_FP_1} --tf ${FAKE_FP_2}

${THIS_FP} --rl ${INCOMP_LISTFILE_FP_1} --tl ${FAKE_FP_2}

${THIS_FP} --rl ${FAKE_FP_1} --tl ${INCOMP_LISTFILE_FP_2}

${THIS_FP} --rl ${INCOMP_LISTFILE_FP_1} --tl ${INCOMP_LISTFILE_FP_2}

${THIS_FP} -H

${THIS_FP} -H --rf ${FAKE_FP_1} --tf ${FAKE_FP_2}

${THIS_FP} -H --rf ${INCOMP_FILE_FP_1} --tf ${INCOMP_FILE_FP_1}

${THIS_FP} -H --rf ${INCOMP_FILE_FP_1} --tl ${INCOMP_FILE_FP_1}

${THIS_FP} -V

${THIS_FP} -V --rl ${FAKE_FP_1} --tl ${FAKE_FP_2}

${THIS_FP} -V --rl ${INCOMP_LISTFILE_FP_2} --tl ${INCOMP_LISTFILE_FP_2}

${THIS_FP} -H -S

${THIS_FP} -H -r ${FAKE_FP_1} --tf ${FAKE_FP_2} -S

${THIS_FP} -H -r ${INCOMP_LISTFILE_FP_1} --tf ${INCOMP_LISTFILE_FP_2} -S

${THIS_FP} -H -V

${THIS_FP} -H --rl ${FAKE_FP_1} --tl ${FAKE_FP_2} -V

${THIS_FP} -H --rl ${INCOMP_LISTFILE_FP_1} --tl ${INCOMP_LISTFILE_FP_1} -V

${THIS_FP} -HV

${THIS_FP} --rf ${FAKE_FP_1} -t ${FAKE_FP_2} -HV

${THIS_FP} --rf ${INCOMP_FILE_FP_2} -t ${INCOMP_FILE_FP_2} -HV

${THIS_FP} -S -V

${THIS_FP} --rl ${FAKE_FP_1} --tl ${FAKE_FP_2} -S -V

${THIS_FP} --rl ${INCOMP_LISTFILE_FP_1} --tl ${INCOMP_LISTFILE_FP_2} -S -V

${THIS_FP} -SV

${THIS_FP} -SV -r ${FAKE_FP_1} -t ${FAKE_FP_2}

${THIS_FP} -SV -r ${INCOMP_FILE_FP_1} -t ${INCOMP_FILE_FP_1}

${THIS_FP} -T

${THIS_FP} --rl ${FAKE_FP_1} --tl ${FAKE_FP_2} -T

${THIS_FP} --rl ${INCOMP_LISTFILE_FP_2} --tl ${INCOMP_LISTFILE_FP_2} -T

${THIS_FP} -HVT

${THIS_FP} -HVT --reference-file ${FAKE_FP_1} --tf ${FAKE_FP_2}

${THIS_FP} -HVT --reference-file ${INCOMP_FILE_FP_1} --tf ${INCOMP_FILE_FP_2}

${THIS_FP} -HST --reference-file ${FAKE_FP_1} -t ${FAKE_FP_2}

${THIS_FP} -HST --reference-file ${INCOMP_FILE_FP_1} -t ${INCOMP_FILE_FP_1}

${THIS_FP} -VST -r ${FAKE_FP_1} --test-file ${FAKE_FP_2}

${THIS_FP} -VST -r ${INCOMP_FILE_FP_2} --test-file ${INCOMP_FILE_FP_2}

${THIS_FP} -SVH -r ${FAKE_FP_1} -t ${FAKE_FP_2}

${THIS_FP} -ST -r ${FAKE_FP_1} -t ${FAKE_FP_2}

${THIS_FP} -ST -r ${INCOMP_FILE_FP_1} -t ${INCOMP_FILE_FP_2}

${THIS_FP} -SVH -r ${INCOMP_FILE_FP_1} -t ${INCOMP_FILE_FP_2}

# ${THIS_FP} --loglevel

# ${THIS_FP} --loglevel 11

# ${THIS_FP} --loglevel=11
# #> Parsing option: '--loglevel', value: '11'

# ----------------------------------------------------------------------
# tests with golden inputs and outputs
# ----------------------------------------------------------------------

### Only "the important bits" (usually @ end) of the output are recorded below.
### You may want to comment calls to functions={print_usage , print_options} to improve output readability. Their output is omitted below, or substituted by '$(print_usage)'.
### I added the 'success!' below for clarity, since the code is setup to succeed silently. TODO: add switch! or make --debug succeed verbosely.

bash --version | head -n 1
# GNU bash, version 3.2.25(1)-release (x86_64-redhat-linux-gnu)

${THIS_FP}
# IOAPIST_driver.sh::evaluate_options: ERROR: found no valid arguments, exiting ...

${THIS_FP} -f
# IOAPIST_driver.sh: ERROR: non-option argument: '-f', exiting ...

${THIS_FP} -f foo
# IOAPIST_driver.sh: ERROR: non-option argument: '-f', exiting ...

${THIS_FP} foo
# IOAPIST_driver.sh: ERROR: found no valid arguments, exiting ...

${THIS_FP} foo bar
# IOAPIST_driver.sh: ERROR: found no valid arguments, exiting ...

### TODO: give better advice
${THIS_FP} --very-bad
# IOAPIST_driver.sh: ERROR: found no valid arguments, exiting ...

### TODO: give better advice
${THIS_FP} --Very-bad
# IOAPIST_driver.sh: ERROR: found no valid arguments, exiting ...

${THIS_FP} -h
$(print_usage)

${THIS_FP} --help
$(print_usage)

${THIS_FP} --rf ${FAKE_FP_1}
# IOAPIST_driver.sh::evaluate_options: ERROR: specified path to reference file without path to test file, exiting ...

### TODO: give better advice
${THIS_FP} -rf ${FAKE_FP_1}
# IOAPIST_driver.sh::evaluate_options: ERROR: non-option argument: '-f', exiting ...

${THIS_FP} -r ${FAKE_FP_1}
# IOAPIST_driver.sh::evaluate_options: ERROR: specified path to reference file without path to test file, exiting ...

${THIS_FP} --tf ${FAKE_FP_2}
# IOAPIST_driver.sh::evaluate_options: ERROR: specified path to test file without path to reference file, exiting ...

### TODO: give better advice
${THIS_FP} -tf ${FAKE_FP_2}
# IOAPIST_driver.sh::evaluate_options: ERROR: non-option argument: '-f', exiting ...

${THIS_FP} -t ${FAKE_FP_2}
# IOAPIST_driver.sh::evaluate_options: ERROR: specified path to test file without path to reference file, exiting ...

${THIS_FP} --rf ${FAKE_FP_1} --tf ${FAKE_FP_2}
# IOAPIST_driver.sh::check_refr_vs_test_filepaths: ERROR: cannot read reference file='/tmp/tmp.wCTfo30872', exiting

${THIS_FP} --rf ${INCOMP_FILE_FP_1} --tf ${FAKE_FP_2}
# IOAPIST_driver.sh::check_refr_vs_test_filepaths: ERROR: cannot read file under test='/tmp/tmp.ffNbC30874', exiting

${THIS_FP} --rf ${FAKE_FP_1} -t ${INCOMP_FILE_FP_2}
# IOAPIST_driver.sh::check_refr_vs_test_filepaths: ERROR: cannot read reference file='/tmp/tmp.wCTfo30872', exiting

${THIS_FP} -r ${INCOMP_FILE_FP_1} --tf ${INCOMP_FILE_FP_1}
# About to run command="REFERENCE_FP='/project/inf35w/roche/met/24-layer/WRF_2008_24aL/12US1/mcip_out/METBDY3D_071222' TEST_FP='/project/inf35w/roche/met/24-layer/WRF_2008_24aL/12US1/mcip_out/METBDY3D_071222' ncl -n /home/roche/code/regridding/IOAPI_spatiotemporality_checker/IOAPIST_ref_vs_test.ncl"
# success!

${THIS_FP} -r ${INCOMP_FILE_FP_1} -t ${INCOMP_FILE_FP_2}
# /home/roche/code/regridding/IOAPI_spatiotemporality_checker/IOAPIST_ref_vs_test.ncl::compare_array_globattr_by_name: ERROR: file under test='/project/inf35w/roche/BC/2008cdc/AQ_raw/BCON_CB05AE5_US12_2007356' and reference file='/project/inf35w/roche/met/24-layer/WRF_2008_24aL/12US1/mcip_out/METBDY3D_071222' have different values in global attribute='VGLVLS':

### TODO: give better advice
${THIS_FP} -tl ${FAKE_FP_1}
# IOAPIST_driver.sh: ERROR: non-option argument: '-l', exiting ...

${THIS_FP} --rl ${FAKE_FP_1}
# IOAPIST_driver.sh::evaluate_options: ERROR: specified path to list of reference files without path to list of test files, exiting ...

${THIS_FP} --tl ${FAKE_FP_2}
# IOAPIST_driver.sh::evaluate_options: ERROR: specified path to list of test files without path to list of reference files, exiting ...

${THIS_FP} --rl ${FAKE_FP_1} --tl ${FAKE_FP_2}
# IOAPIST_driver.sh::check_refr_vs_test_listfiles: ERROR: cannot read list of reference files='/tmp/tmp.wCTfo30872', exiting

${THIS_FP} --rf ${FAKE_FP_1} --tl ${FAKE_FP_2}
# IOAPIST_driver.sh::evaluate_options: ERROR: specified path to reference file without path to test file, exiting ...

${THIS_FP} --rl ${FAKE_FP_1} --tf ${FAKE_FP_2}
# IOAPIST_driver.sh::evaluate_options: ERROR: specified path to test file without path to reference file, exiting ...

${THIS_FP} --rl ${INCOMP_LISTFILE_FP_1} --tl ${FAKE_FP_2}
# IOAPIST_driver.sh::check_refr_vs_test_listfiles: ERROR: cannot read list of test file='/tmp/tmp.ffNbC30874', exiting

${THIS_FP} --rl ${FAKE_FP_1} --tl ${INCOMP_LISTFILE_FP_2}
# IOAPIST_driver.sh::check_refr_vs_test_listfiles: ERROR: cannot read list of reference files='/tmp/tmp.wCTfo30872', exiting

${THIS_FP} --rl ${INCOMP_LISTFILE_FP_1} --tl ${INCOMP_LISTFILE_FP_2}
# /home/roche/code/regridding/IOAPI_spatiotemporality_checker/IOAPIST_reflist_vs_testlist.ncl::compare_array_globattr_by_name: ERROR: file under test='/project/inf35w/roche/BC/2008cdc/AQ_raw/BCON_CB05AE5_US12_2007356' and reference file='/project/inf35w/roche/met/24-layer/WRF_2008_24aL/12US1/mcip_out/METBDY3D_071222' have different values in global attribute='VGLVLS':

${THIS_FP} -H
# IOAPIST_driver.sh::evaluate_options: ERROR: will not process either simple files or listfiles, so nothing to do! exiting ...

${THIS_FP} -H --rf ${FAKE_FP_1} --tf ${FAKE_FP_2}
# IOAPIST_driver.sh::check_refr_vs_test_filepaths: ERROR: cannot read reference file='/tmp/tmp.wCTfo30872', exiting

${THIS_FP} -H --rf ${INCOMP_FILE_FP_1} --tf ${INCOMP_FILE_FP_1}
# About to run command="NO_HORIZONTAL='True' REFERENCE_FP='/project/inf35w/roche/met/24-layer/WRF_2008_24aL/12US1/mcip_out/METBDY3D_071222' TEST_FP='/project/inf35w/roche/met/24-layer/WRF_2008_24aL/12US1/mcip_out/METBDY3D_071222' ncl -n /home/roche/code/regridding/IOAPI_spatiotemporality_checker/IOAPIST_ref_vs_test.ncl"
# success!

${THIS_FP} -H --rf ${INCOMP_FILE_FP_1} --tl ${INCOMP_FILE_FP_1}
# IOAPIST_driver.sh::evaluate_options: ERROR: specified path to reference file without path to test file, exiting ...

${THIS_FP} -V
# IOAPIST_driver.sh::evaluate_options: ERROR: will not process either simple files or listfiles, so nothing to do! exiting ...

${THIS_FP} -V --rl ${FAKE_FP_1} --tl ${FAKE_FP_2}
# IOAPIST_driver.sh::check_refr_vs_test_listfiles: ERROR: cannot read list of reference files='/tmp/tmp.wCTfo30872', exiting

${THIS_FP} -V --rl ${INCOMP_LISTFILE_FP_2} --tl ${INCOMP_LISTFILE_FP_2}
# About to run command="NO_VERTICAL=True LIST_OF_REFR_FILES_FP='/home/roche/code/regridding/IOAPI_spatiotemporality_checker/test_files.txt' LIST_OF_TEST_FILES_FP='/home/roche/code/regridding/IOAPI_spatiotemporality_checker/test_files.txt' ncl -n /home/roche/code/regridding/IOAPI_spatiotemporality_checker/IOAPIST_reflist_vs_testlist.ncl"
# success!

${THIS_FP} -H -S
# IOAPIST_driver.sh::evaluate_options: ERROR: will not process either simple files or listfiles, so nothing to do! exiting ...

${THIS_FP} -H -r ${FAKE_FP_1} --tf ${FAKE_FP_2} -S
# IOAPIST_driver.sh::check_refr_vs_test_filepaths: ERROR: cannot read reference file='/tmp/tmp.wCTfo30872', exiting

# TODO: give better error, or prevent NCL from flailing--it tries to handle .txt!
${THIS_FP} -H -r ${INCOMP_LISTFILE_FP_1} --tf ${INCOMP_LISTFILE_FP_2} -S
# IOAPIST_driver.sh::check_refr_vs_test_filepaths::REFERENCE_FP='/home/roche/code/regridding/IOAPI_spatiotemporality_checker/refr_files.txt' TEST_FP='/home/roche/code/regridding/IOAPI_spatiotemporality_checker/test_files.txt' ncl -n /home/roche/code/regridding/IOAPI_spatiotemporality_checker/IOAPIST_ref_vs_test.ncl: ERROR: failed or not found

${THIS_FP} -H -V
# IOAPIST_driver.sh::evaluate_options: ERROR: will not process either simple files or listfiles, so nothing to do! exiting ...

${THIS_FP} -H --rl ${FAKE_FP_1} --tl ${FAKE_FP_2} -V
# IOAPIST_driver.sh::check_refr_vs_test_listfiles: ERROR: cannot read list of reference files='/tmp/tmp.wCTfo30872', exiting

${THIS_FP} -H --rl ${INCOMP_LISTFILE_FP_1} --tl ${INCOMP_LISTFILE_FP_1} -V
# About to run command="NO_VERTICAL='True' NO_HORIZONTAL='True' LIST_OF_REFR_FILES_FP='/home/roche/code/regridding/IOAPI_spatiotemporality_checker/refr_files.txt' LIST_OF_TEST_FILES_FP='/home/roche/code/regridding/IOAPI_spatiotemporality_checker/refr_files.txt' ncl -n /home/roche/code/regridding/IOAPI_spatiotemporality_checker/IOAPIST_reflist_vs_testlist.ncl"
# success!

${THIS_FP} -HV
# IOAPIST_driver.sh::evaluate_options: ERROR: will not process either simple files or listfiles, so nothing to do! exiting ...

${THIS_FP} --rf ${FAKE_FP_1} -t ${FAKE_FP_2} -HV
# IOAPIST_driver.sh::check_refr_vs_test_filepaths: ERROR: cannot read reference file='/tmp/tmp.wCTfo30872', exiting

${THIS_FP} --rf ${INCOMP_FILE_FP_2} -t ${INCOMP_FILE_FP_2} -HV
# About to run command="NO_VERTICAL='True' NO_HORIZONTAL='True' REFERENCE_FP='/project/inf35w/roche/BC/2008cdc/AQ_raw/BCON_CB05AE5_US12_2007356' TEST_FP='/project/inf35w/roche/BC/2008cdc/AQ_raw/BCON_CB05AE5_US12_2007356' ncl -n /home/roche/code/regridding/IOAPI_spatiotemporality_checker/IOAPIST_ref_vs_test.ncl"
# success!

${THIS_FP} -S -V
# IOAPIST_driver.sh::evaluate_options: ERROR: will not process either simple files or listfiles, so nothing to do! exiting ...

${THIS_FP} --rl ${FAKE_FP_1} --tl ${FAKE_FP_2} -S -V
# IOAPIST_driver.sh::check_refr_vs_test_listfiles: ERROR: cannot read list of reference files='/tmp/tmp.wCTfo30872', exiting

${THIS_FP} --rl ${INCOMP_LISTFILE_FP_1} --tl ${INCOMP_LISTFILE_FP_2} -S -V
# About to run command="NO_VERTICAL='True' NO_HORIZONTAL='True' LIST_OF_REFR_FILES_FP='/home/roche/code/regridding/IOAPI_spatiotemporality_checker/refr_files.txt' LIST_OF_TEST_FILES_FP='/home/roche/code/regridding/IOAPI_spatiotemporality_checker/test_files.txt' ncl -n /home/roche/code/regridding/IOAPI_spatiotemporality_checker/IOAPIST_reflist_vs_testlist.ncl"
# success!

${THIS_FP} -SV
# IOAPIST_driver.sh::evaluate_options: ERROR: will not process either simple files or listfiles, so nothing to do! exiting ...

${THIS_FP} -SV -r ${FAKE_FP_1} -t ${FAKE_FP_2}
# IOAPIST_driver.sh::check_refr_vs_test_filepaths: ERROR: cannot read reference file='/tmp/tmp.wCTfo30872', exiting

${THIS_FP} -SV -r ${INCOMP_FILE_FP_1} -t ${INCOMP_FILE_FP_1}
# About to run command="NO_VERTICAL='True' NO_HORIZONTAL='True' REFERENCE_FP='/project/inf35w/roche/met/24-layer/WRF_2008_24aL/12US1/mcip_out/METBDY3D_071222' TEST_FP='/project/inf35w/roche/met/24-layer/WRF_2008_24aL/12US1/mcip_out/METBDY3D_071222' ncl -n /home/roche/code/regridding/IOAPI_spatiotemporality_checker/IOAPIST_ref_vs_test.ncl"
# success!

${THIS_FP} -T
# IOAPIST_driver.sh::evaluate_options: ERROR: will not process either simple files or listfiles, so nothing to do! exiting ...

${THIS_FP} --rl ${FAKE_FP_1} --tl ${FAKE_FP_2} -T
# IOAPIST_driver.sh::check_refr_vs_test_listfiles: ERROR: cannot read list of reference files='/tmp/tmp.wCTfo30872', exiting

${THIS_FP} --rl ${INCOMP_LISTFILE_FP_2} --tl ${INCOMP_LISTFILE_FP_2} -T
# About to run command="NO_TEMPORAL='True' LIST_OF_REFR_FILES_FP='/home/roche/code/regridding/IOAPI_spatiotemporality_checker/test_files.txt' LIST_OF_TEST_FILES_FP='/home/roche/code/regridding/IOAPI_spatiotemporality_checker/test_files.txt' ncl -n /home/roche/code/regridding/IOAPI_spatiotemporality_checker/IOAPIST_reflist_vs_testlist.ncl"
# success!

${THIS_FP} -HVT
# IOAPIST_driver.sh::evaluate_options: ERROR: will not process either simple files or listfiles, so nothing to do! exiting ...

${THIS_FP} -HVT --reference-file ${FAKE_FP_1} --tf ${FAKE_FP_2}
# IOAPIST_driver.sh::evaluate_options: ERROR: will not process spatially or temporally, so nothing to do! exiting ...

${THIS_FP} -HVT --reference-file ${INCOMP_FILE_FP_1} --tf ${INCOMP_FILE_FP_2}
# IOAPIST_driver.sh::evaluate_options: ERROR: will not process spatially or temporally, so nothing to do! exiting ...

${THIS_FP} -HST --reference-file ${FAKE_FP_1} -t ${FAKE_FP_2}
# IOAPIST_driver.sh::evaluate_options: ERROR: will not process spatially or temporally, so nothing to do! exiting ...

${THIS_FP} -HST --reference-file ${INCOMP_FILE_FP_1} -t ${INCOMP_FILE_FP_1}
# IOAPIST_driver.sh::evaluate_options: ERROR: will not process spatially or temporally, so nothing to do! exiting ...

${THIS_FP} -VST -r ${FAKE_FP_1} --test-file ${FAKE_FP_2}
# IOAPIST_driver.sh::evaluate_options: ERROR: will not process spatially or temporally, so nothing to do! exiting ...

${THIS_FP} -VST -r ${INCOMP_FILE_FP_2} --test-file ${INCOMP_FILE_FP_2}
# IOAPIST_driver.sh::evaluate_options: ERROR: will not process spatially or temporally, so nothing to do! exiting ...

${THIS_FP} -SVH -r ${FAKE_FP_1} -t ${FAKE_FP_2}
# IOAPIST_driver.sh::check_refr_vs_test_filepaths: ERROR: cannot read reference file='/tmp/tmp.wCTfo30872', exiting

${THIS_FP} -ST -r ${FAKE_FP_1} -t ${FAKE_FP_2}
# IOAPIST_driver.sh::evaluate_options: ERROR: will not process spatially or temporally, so nothing to do! exiting ...

${THIS_FP} -ST -r ${INCOMP_FILE_FP_1} -t ${INCOMP_FILE_FP_2}
# IOAPIST_driver.sh::evaluate_options: ERROR: will not process spatially or temporally, so nothing to do! exiting ...

${THIS_FP} -SVH -r ${INCOMP_FILE_FP_1} -t ${INCOMP_FILE_FP_2}
# About to run command="NO_VERTICAL='True' NO_HORIZONTAL='True' REFERENCE_FP='/project/inf35w/roche/met/24-layer/WRF_2008_24aL/12US1/mcip_out/METBDY3D_071222' TEST_FP='/project/inf35w/roche/BC/2008cdc/AQ_raw/BCON_CB05AE5_US12_2007356' ncl -n /home/roche/code/regridding/IOAPI_spatiotemporality_checker/IOAPIST_ref_vs_test.ncl"
# success!
