*(part of the [AQMEII-NA_N2O][AQMEII-NA_N2O wiki home] family of projects)*

**table of contents**

[TOC]

# open-source notice

Copyright 2013, 2014 Tom Roche <Tom_Roche@pobox.com>

This project's content is free software: you can redistribute it and/or modify it provided that you do so as follows:

* under the terms of the [GNU Affero General Public License][GNU Affero General Public License HTML @ GNU] as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

This project's content is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the [GNU Affero General Public License][GNU Affero General Public License local text] for more details.

![distributed under the GNU Affero General Public License](../../downloads/Affero_badge__agplv3-155x51.png)

[GNU Affero General Public License local text]: ./COPYING
[GNU Affero General Public License HTML @ GNU]: https://www.gnu.org/licenses/agpl.html

# summary

Suppose you have [IOAPI][] files (aka IOAPI-flavored [netCDF][netCDF @ wikipedia]) defined over one or more spatiotemporal domains (e.g., [`AQMEII-NA`][AQMEII-NA spatial domain]. [NCL][NCL @ wikipedia] code in this project compares the grid (whether 2D or 3D) defined in a *reference* netCDF file against that defined in another netCDF file (i.e., a file under *test*). The NCL code can similarly loop over sets of files supplied by the user as list-of-FQ-path text files (or *listfiles*). A [bash][bash @ wikipedia] driver allows the user to

* ignore the NCL code. Users interact directly only with the driver script, not the NCL--no programming required!
* feed the NCL code with paths to the relevant reference and test files, or to the relevant listfiles
* *gate* (or turn off) horizontal, vertical, or temporal tests. Thus, for example, one can compare the horizontal grids of a 2D file to that of a 3D file by gating the vertical tests.

The driver exits with helpful prompt on error (e.g., if the test file's grid does not match that of the reference), or no error on success.

Requires `ncl` (the NCL interpreter) in your ``$PATH``.

[AQMEII-NA_N2O wiki home]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/Home
[AQMEII-NA spatial domain]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/AQMEII-NA_spatial_domain
[IOAPI]: https://www.cmascenter.org/ioapi/
[netCDF @ wikipedia]: http://en.wikipedia.org/wiki/NetCDF#Format_description
[NCL @ wikipedia]: http://en.wikipedia.org/wiki/NCAR_Command_Language
[bash @ wikipedia]: http://en.wikipedia.org/wiki/Bash_%28Unix_shell%29
[IOAPIST_ref_vs_test.ncl]: ../../src/HEAD/IOAPIST_ref_vs_test.ncl?at=master

# objective

[CCTM][CCTM @ CMAQ wiki] provides what appears to be very similar functionality to do spatiotemporal checking (STC) of inputs: in fact, I strongly suspect IOAPIST does nothing that can't also be done by running CCTM on your inputs. However, CCTM does STC with [IOAPI][]'s [`GRDCHK3`][GRDCHK3] routine, which is Fortran. Using it therefore requires either

1. running CCTM. If you already have CCTM setup, this is less of a problem, but CCTM is a **big** piece of software: running it is always non-trivial, and seems to this author like overkill just to do a spatiotemporality check.
2. creating a separate Fortran compilation unit for `GRDCHK3` and its dependencies. Again, a substantial piece of work for this rather limited task.

IOAPIST seeks mostly to do STC for your inputs, and to do so

1. more easily (i.e., with substantially less user effort) than CCTM
1. more quickly than CCTM
1. just as accurately as CCTM

IOAPIST also might prove useful as NCL or bash example code, for those who care to read the sources, but that's a distinctly secondary objective.

[CCTM @ CMAQ wiki]: http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_(February_2010_release)_OGD#CCTM
[GRDCHK3]: https://www.cmascenter.org/ioapi/documentation/3.1/html/GRDCHK3.html
[CMAQ @ CMAS]: http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_(February_2010_release)_OGD#CCTM

# description

Uses [bash][bash @ wikipedia] to drive [NCL][NCL @ wikipedia] code to test the spatiotemporal consistency either of

1. one test file against one reference file
1. one file containing a list of FQ paths to test files (aka *test listfile*, 1 path per line, no other text) against another file containing a list of FQ paths to reference files (aka *reference listfile*, also 1 path/line, no other text). Each pair of files denoted by the paths will then be tested one at a time: the first path/line from the test listfile vs the first path/line from the reference listfile, and so on.

and to test one or more of the following aspects: horizontal, vertical, spatial (==horizontal+vertical), temporal. GNU-style long and short commandline options determine [driver][IOAPIST_driver.sh] behavior:

    ================   ============   =============================================
    long option        short option   semantics
    ================   ============   =============================================
    --help             -h             print help message
    --reference-file   -r             followed by path to (single) reference file
    --rf                              abbreviates --reference-file
    --reference-list   [none]         followed by path to reference listfile
    --rl                              abbreviates --reference-list
    --test-file        -t             followed by path to (single) test file
    --tf                              abbreviates --test-file
    --test-list        [none]         followed by path to test listfile
    --tl                              abbreviates --test-list
    --no-horizontal    -H             run no horizontal tests
    --no-temporal      -T             run no temporal tests
    --no-vertical      -V             run no vertical tests
    --no-spatial       -S             implies --no-horizontal && --no-vertical
    ================   ============   =============================================

Tests use [NCL code][IOAPIST_helpers.ncl] to compare [IOAPI][] metadata in the [netCDF][netCDF @ wikipedia] files. Default behavior is to run all tests (unless specifically gated) on the provided pair of files, but the paths to the pair of files must be provided, using the options above: no naked or positional parameters are accepted. Jobs fail fast, exiting on first failure. For example syntax, and its expected behavior, consult test cases at end of the [driver][IOAPIST_driver.sh] (after final `exit 0`). 

[IOAPIST_driver.sh]: ../../src/HEAD/IOAPIST_driver.sh?at=master

# installation

1. IOAPIST requires `ncl` (the NCL interpreter) in your ``$PATH``. Fortunately, [installing NCL][NCL download/install page] is almost trivial--**much** simpler than installing CCTM.
1. Choose a directory in which to install IOAPIST. For these instructions, I'll use `/path/to/IOAPIST/`
1. Download this repository to the directory of your choice. Two ways to do that are:
    1. [`git clone`][`git clone` @ the Git book] this repository. Note that, if your platform blocks external SSH (e.g., if you're at EPA), you'll need to use the HTTP protocol: i.e., `git clone http://bitbucket.org/tlroche/ioapi_spatiotemporality_checker.git /path/to/IOAPIST/`
    1. Download this repository as a zipfile: go [here][IOAPIST>Downloads] and click the link to `Download repository`. Then, unzip it :-)

At this point, you can either

* call the driver directly, either
    * absolutely (e.g., `/path/to/IOAPIST/IOAPIST_driver.sh <your options here/>`) or relatively or
    * relatively
* put your IOAPIST directory in your ``$PATH``

[NCL download/install page]: http://www.ncl.ucar.edu/Download/
[IOAPIST>Downloads]: ../../downloads
[`git clone` @ the Git book]: http://git-scm.com/book/en/Git-Basics-Getting-a-Git-Repository#Cloning-an-Existing-Repository

# examples

## simple file check

To test simple files (i.e., one file path vs another) use 

    /path/to/IOAPIST_driver.sh -r /path/to/reference/file -t /path/to/test/file

(or the synonymous long options) with any desired test gates.

## listfile check

To test listfiles, use

    /path/to/IOAPIST_driver.sh --rl /path/to/reference/filelist --tl /path/to/test/filelist

(or the other long options) with any desired test gates. Listfiles can be made by hand (in a text editor), by shell redirection (see examples below), or by any other means sufficient to build a text file containing one fully-qualified file path per line. Just be sure to make the reference and test listfiles the same length: i.e., each should have the same number of lines.

## boundary conditions vs meteorology

I sought to compare a set of BC files to a set of meteorology files. Both sets were daily, and in different directories/folders, but the met files came in several varieties (`GRIDCRO2D`, `METBDY3D`, etc). My problems were

* generally: how to create listfiles from the directories?
* specifically: separating BC files from IC files, which were mixed in the same directory.

I built the listfiles (`BC_LISTFILE_FP` and `MET_LISTFILE_FP` below, in "scratch" directories, to avoid "polluting" the data directories) for the comparison using the following bash scriptlet:

    MET_DIR='/project/inf35w/roche/met/24-layer/WRF_2008_24aL/12US1/mcip_out'
    MET_LISTFILE_FP="/tmp${MET_DIR}/met_listfile.txt"
    BC_DIR='/project/inf35w/roche/BC/2008cdc/AQ_raw'
    BC_LISTFILE_FP="/tmp${BC_DIR}/bc_listfile.txt"

    ### Use METBDY3D files for reference, since they must also be used for vertical check.
    if [[ ! -r "${MET_LISTFILE_FP}" ]] ; then
      for CMD in \
        "mkdir -p ${MET_LISTFILE_DIR}/" \
        "find ${MET_DIR} -maxdepth 1 -name 'METBDY3D_??????' | sort > ${MET_LISTFILE_FP}" \
        "wc -l ${MET_LISTFILE_FP}" \
        "head ${MET_LISTFILE_FP}" \
        "tail ${MET_LISTFILE_FP}" \
      ; do
        echo -e "$ ${CMD}"
        eval "${CMD}"
      done
    else
      # delete the file if not wanted
      echo -e "met listfile='"${MET_LISTFILE_FP}"' exists, not overwriting"
    fi

    if [[ ! -r "${BC_LISTFILE_FP}" ]] ; then
      BC_LISTFILE_DIR="$(dirname "${BC_LISTFILE_FP}")"
      for CMD in \
        "mkdir -p ${BC_LISTFILE_DIR}/" \
        "find ${BC_DIR} -maxdepth 1 -name 'BCON*_???????' | sort > ${BC_LISTFILE_FP}" \
        "wc -l ${BC_LISTFILE_FP}" \
        "head ${BC_LISTFILE_FP}" \
        "tail ${BC_LISTFILE_FP}" \
      ; do
        echo -e "$ ${CMD}"
        eval "${CMD}"
      done
    else
      # delete the file if not wanted
      echo -e "BC listfile='"${BC_LISTFILE_FP}"' exists, not overwriting"
    fi

I then did the ST check, using the meteorology as reference:

    ~/code/regridding/ioapi_spatiotemporality_checker/IOAPIST_driver.sh --rl ${MET_LISTFILE_FP} --tl ${BC_LISTFILE_FP}

The checks abended after the error indicated by console output including the following line:

    /home/roche/code/regridding/ioapi_spatiotemporality_checker/IOAPIST_reflist_vs_testlist.ncl::compare_array_globattr_by_name: ERROR: file under test='/project/inf35w/roche/BC/2008cdc/AQ_raw/BCON_CB05AE5_US12_2007356' and reference file='/project/inf35w/roche/met/24-layer/WRF_2008_24aL/12US1/mcip_out/METBDY3D_071222' have different values in global attribute='VGLVLS':

## emissions vs meteorology

I sought to compare a set of daily emissions files to a set of daily meteorology files. My problems were

* generally: how to create listfiles from the directories?
* specifically: separating emissions netCDF from other files mixed in the same directory.

I built the listfiles (`EMI_LISTFILE_FP` and `MET_LISTFILE_FP` below, in "scratch" directories, to avoid "polluting" the data directories) for the comparison using the following bash scriptlet:

    MET_DIR='/project/inf35w/roche/met/35-layer/WRF_2008_35aL/12US1_wetlands100/mcip_out'
    MET_LISTFILE_FP="/tmp${MET_DIR}/met_listfile.txt"
    EMI_DIR='/project/inf35w/roche/emis/2008ab.ag_v2/AQ_raw'
    EMI_LISTFILE_FP="/tmp${EMI_DIR}/emi_listfile.txt"

    ### Use METBDY3D files for reference, since they must also be used for vertical check.
    if [[ ! -r "${MET_LISTFILE_FP}" ]] ; then
      for CMD in \
        "mkdir -p ${MET_LISTFILE_DIR}/" \
        "find ${MET_DIR} -maxdepth 1 -name 'METBDY3D_??????' | grep -ve '_09' | sort > ${MET_LISTFILE_FP}" \
        "wc -l ${MET_LISTFILE_FP}" \
        "head ${MET_LISTFILE_FP}" \
        "tail ${MET_LISTFILE_FP}" \
      ; do
        echo -e "$ ${CMD}"
        eval "${CMD}"
      done
    else
      # delete the file if not wanted
      echo -e "met listfile='"${MET_LISTFILE_FP}"' exists, not overwriting"
    fi

    if [[ ! -r "${EMI_LISTFILE_FP}" ]] ; then
      EMI_LISTFILE_DIR="$(dirname "${EMI_LISTFILE_FP}")"
      for CMD in \
        "mkdir -p ${EMI_LISTFILE_DIR}/" \
        "find ${EMI_DIR} -maxdepth 1 | grep -e '_[0-9]\{8\}_' | sort > ${EMI_LISTFILE_FP}" \
        "wc -l ${EMI_LISTFILE_FP}" \
        "head ${EMI_LISTFILE_FP}" \
        "tail ${EMI_LISTFILE_FP}" \
      ; do
        echo -e "$ ${CMD}"
        eval "${CMD}"
      done
    else
      # delete the file if not wanted
      echo -e "emissions listfile='"${EMI_LISTFILE_FP}"' exists, not overwriting"
    fi

I then did the ST check, as usual, using the meteorology as reference, but being sure to turn off vertical checks (since the emissions files are 2D):

    ~/code/regridding/ioapi_spatiotemporality_checker/IOAPIST_driver.sh -V  --rl ${MET_LISTFILE_FP} --tl ${EMI_LISTFILE_FP}

The two sets of files passed the checks: the job ran to completion with no errors.

## testing

There is a sample test fixture and a large number of test cases at the end of [`IOAPIST_driver.sh`][IOAPIST_driver.sh]. Unfortunately this is not fully automated (need for use of a bash xUnit--or to switch to python, which has both NCL-type bindings and several xUnit-style frameworks--noted below), but one can run the fixture scriptlet (editing as necessary for your environment), then run the cases (noting the intended output).

# TODOs

1. Move all TODOs to [issue tracker][IOAPIST issues].
1. Do [`uber_driver.sh`][uber_driver.sh] to simplify driving/testing.
1. [`IOAPIST_ref_vs_test.ncl`][IOAPIST_ref_vs_test.ncl], [`IOAPIST_reflist_vs_testlist.ncl`][IOAPIST_reflist_vs_testlist.ncl]: refactor duplicated test-gate-parsing code.
1. Give better progress indication when testing listfiles. Currently, the console just sits there, which might appear to be a hang (esp if running remotely).
1. Signal success? or at least provide a switch for user who wants positive reinforcement (rather than the absence of negative reinforcement, currently provided on success).
1. Convert this file into [reStructuredText](https://en.wikipedia.org/wiki/reStructuredText) to allow a real table (instead of the blockquote above).
1. For testing:
    1. Test procedures in [`IOAPIST_helpers.ncl`][IOAPIST_helpers.ncl] should return ADT indicating failure and mode, not just exit.
    1. [Driver][IOAPIST_driver.sh] should read the failure ADT, print helpful message (scavenge from current [`IOAPIST_helpers.ncl`][IOAPIST_helpers.ncl]), then exit.
    1. Make mocks for particular errors: this is straightforward with NCL (but labor intensive).
    1. Use a test framework:
        1. While using [bash][bash @ wikipedia]: consider [bashunit](https://github.com/djui/bashunit) or [shUnit2](https://code.google.com/p/shunit2/wiki/ProjectInfo) (unfortunately `subversion`-hosted) or others listed [here](https://en.wikipedia.org/wiki/List_of_unit_testing_frameworks#Shell).
        1. Or switch to python! for which there are many options.
1. *(meta-project)* [Driver][IOAPIST_driver.sh] should take switches for debugging, no/eval, etc.
1. *(meta-project)* Update all IOAPI links from `baronams.com` to `cmascenter.org`.
1. [`IOAPIST_helpers.ncl`][IOAPIST_helpers.ncl]:
    1. move generic functions/procedures to [`regrid_utils`][regrid_utils], e.g., `dim_name_to_dim_index` -> `regrid_utils/dimensions.ncl`
    1. Move generic filepath utilities currently in `./IOAPIST_helpers.ncl` and `regrid_utils/get_filepath_from_template.ncl` to `regrid_utils/filepaths.ncl`. Particularly, move `IOAPIST_helpers.ncl::get_filepaths_from_file`, which has an exact copy @ [`CMAQ_BC_IC_plus_N2O/BC_IC_helpers.ncl`][CMAQ_BC_IC_plus_N2O/BC_IC_helpers.ncl].
    1. use [`regrid_utils`][regrid_utils]
    1. `compare_array*` methods should show differences better: don't just show the entire array.
1. Bug [ncl-talk][list=ncl-talk info page] about inability to define a default `load`. See [`IOAPIST_ref_vs_test.ncl`][IOAPIST_ref_vs_test.ncl].

[IOAPIST issues]: ../../issues
[regrid_utils]: https://bitbucket.org/epa_n2o_project_team/regrid_utils
[resource_strings.sh]: ../../src/HEAD/resource_strings.sh?at=master
[uber_driver.sh]: ../../src/HEAD/uber_driver.sh?at=master
[IOAPIST_helpers.ncl]: ../../src/HEAD/IOAPIST_helpers.ncl?at=master
[list=ncl-talk info page]: http://mailman.ucar.edu/mailman/listinfo/ncl-talk
[CMAQ_BC_IC_plus_N2O/BC_IC_helpers.ncl]: https://bitbucket.org/epa_n2o_project_team/cmaq_bc_ic_plus_n2o/src/HEAD/BC_IC_helpers.ncl?at=master
[IOAPIST_reflist_vs_testlist.ncl]: ../../src/HEAD/IOAPIST_reflist_vs_testlist.ncl?at=master
