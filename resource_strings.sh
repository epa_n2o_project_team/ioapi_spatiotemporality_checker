### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### For bash `source`ing only, to setup envvars used in both *_driver.sh and uber_driver.sh

# ----------------------------------------------------------------------
# constants with some simple manipulations
# ----------------------------------------------------------------------

### who we are. Does THIS work under `source`?
THIS="$0"
THIS_FN="$(basename ${THIS})"
THIS_DIR="$(readlink -f $(dirname ${THIS}))"

### details of this project
LOCAL_PROJECT_NAME='IOAPI_spatiotemporality_checker' # TODO: get from commandline
## where this is hosted: follow bitbucket conventions
REMOTE_PROJECT_NAME="$(echo ${LOCAL_PROJECT_NAME} | tr '[:upper:]' '[:lower:]')" # bash >= 4: "${LOCAL_PROJECT_NAME,,}"
REMOTE_PROJECT_GIT_URI="git@bitbucket.org:tlroche/${REMOTE_PROJECT_NAME}.git"
REMOTE_PROJECT_HTTP_URI="https://bitbucket.org/epa_n2o_project_team/${REMOTE_PROJECT_NAME}"

# ### details of this project's code dependency
# # path to a project, not a .git
# REGRID_UTILS_URI='https://bitbucket.org/epa_n2o_project_team/regrid_utils'
# REGRID_UTILS_PN="$(basename ${REGRID_UTILS_URI})" # project name
# # can also use 'git@bitbucket.org:tlroche/regrid_utils' if supported

### workspace
#export PROJECT_DIR="${HOME}/code/regridding/${LOCAL_PROJECT_NAME}"
export PROJECT_DIR="${THIS_DIR}"
export WORK_DIR="${PROJECT_DIR}"

### IOAPI metadata:
### see http://www.baronams.com/products/ioapi/INCLUDE.html#fdesc
### implemented in IOAPIST_ref_vs_test.ncl

### inputs, mostly for testing

# # start for testing only

# ## Give FQ paths to lists of FQ paths to files to ST-check.
# ## Comment this out if you want to compare individual files (not lists), since these will override single-path envvars.
# if [[ -z "${LIST_OF_REFR_FILES_FP}" &&
#       -z "${LIST_OF_TEST_FILES_FP}" ]] ; then
#   export LIST_OF_REFR_FILES_FP="${WORK_DIR}/refr_files.txt"
#   export LIST_OF_TEST_FILES_FP="${WORK_DIR}/test_files.txt"
# fi

# ## Give FQ paths to files to ST-check (but only if list files !defined)
# if [[ -z "${LIST_OF_REFR_FILES_FP}" &&
#       -z "${LIST_OF_TEST_FILES_FP}" &&
#       -z "${REFERENCE_FP}"          &&
#       -z "${TEST_FP}" ]] ; then
#   export REFERENCE_FP='/project/inf35w/roche/met/24-layer/WRF_2008_24aL/12US1/mcip_out/METBDY3D_071222'
#   export TEST_FP='/project/inf35w/roche/BC/2008cdc/AQ_raw/BCON_CB05AE5_US12_2007356'
# fi

# #   end for testing only

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

### Shared with driver and uber_driver
function setup_resources {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  for CMD in \
    "mkdir -p ${WORK_DIR}" \
  ; do
    echo -e "\n${MESSAGE_PREFIX}:${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
      exit 1
    fi
  done
} # end function setup_resources
